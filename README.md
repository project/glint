# Glint

Glint is a toolkit that aims to facilitate access of field values for entities.

- [Quickstart](#quickstart)
- [Core Features](#core-features)
  - [Entity Helpers](#entity-helpers)
  - [Entity Helper Collections](#entity-helper-collections)
- [Configuration](#configuration)
- [Advanced Features](#advanced-features)
  - [Glint Service](#glint-service)
  - [Glint Alter Hooks](#glint-alter-hooks)
- [Roadmap](#roadmap)

## Quickstart

Enabling the Glint module **does nothing by default**. You must make use of the
[features](#core-features) described below to get worth out of the module.

This approach guarantees no further overhead from simply enabling the module,
and leaves all judgement up to the developers that acquire and choose to use the
toolkit.

## Core Features

### Entity Helpers

Entity Helpers can be used to facilitate access to entity field values and
obtain cleaner data to work with.

They are designed to be used at the theme layer and make accessing values much
easier than normal Drupal means.

Say you want to access the `body` field of a node. Normally you would do
something like this.

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $node = $variables['node'];
  $body = $node->get('body')->getValue();
}
```

A `dump()` of `$body` would give you something akin to the following.

![Body Traditional Data](./docs/screenshots/body-field-traditional.png)

With a helper, you would do the following.

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $helper = \Drupal\glint\Glint::helper($variables['node']);
  $body = $helper->get('body');
}
```

A `dump()` of `$body` now would give you the same data, albeit slightly cleaner.

![Body Glint Data](./docs/screenshots/body-field-glint.png)

The true worth of Glint isn't for simple text fields, however. It is not
recommended to make use of a helper to get simple values that you can already
obtain from the node fairly easily.

As you saw, Glint did not have much cleaning to do for the `body` field, as the
`->getValue()` function for text field types are quite simple, and already
return what we need.

Glint shines more when trying to access values for Drupal's trickier field
types. We're talking **File** fields, **Media References** and
**Entity References**.

When dealing with reference fields, it can be difficult to access specific
values from a referenced entity.

Below, let's try to traditionally access information and metadata from a
**File Reference** field.

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $node = $variables['node'];
  // Field value gives us the target ID.
  $fileFieldValue = $node->get('field_document')->getValue();
  // We can get the referenced entity like so.
  // Then we can get the data we need from the referenced entity.
  $fileFieldEntity = $node->get('field_document')->entity;
  $fileFieldUri = $fileFieldEntity->getFileUri();
  $fileFieldUrl = \Drupal::service('file_url_generator')->generateAbsoluteString($fileFieldUri);
  $fileFieldFileName = $fileFieldEntity->getFilename();
  $fileFieldMetadata = [
    'filesize' => $fileFieldEntity->getSize(),
    'mimetype' => $fileFieldEntity->getMimeType(),
  ];
}
```

This is commonly accessed information that one would expect to get when
accessing file field values. With a helper, you can achieve the same thing with
fewer lines of code.

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $helper = \Drupal\glint\Glint::helper($variables['node']);
  $file = $helper->get('field_document'); // File reference field.
}
```

With these two lines, the `$file` variable would be populated with the following
output:

![File Glint Data](./docs/screenshots/file-field-glint.png)

The same can be said for **Media Reference** fields, which can be even more
complex to access traditionally than **File References**. This is because
inherently, a **Media Reference** nests image information two layers deep. You
must first access the **Media Entity**, and then the **Image Entity** within it,
normally found in the `field_media_image` field for images.

This leads to the following kind of code:

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $node = $variables['node'];
  $mediaRefFieldValue = $node->get('field_featured_image')->getValue();
  $mediaRefFieldEntity = $node->get('field_featured_image')->entity;
  $mediaRefFieldFileEntity = $mediaRefFieldEntity->get('field_media_image')->entity;
  $mediaRefFieldFileUri = $mediaRefFieldFileEntity->getFileUri();
  $mediaRefFieldFileUrl = \Drupal::service('file_url_generator')->generateAbsoluteString($mediaRefFieldFileUri);
  $mediaRefFieldFileFileName = $mediaRefFieldFileEntity->getFilename();
  $mediaRefFieldFileMetadata = [
    'filesize' => $mediaRefFieldFileEntity->getSize(),
    'mimetype' => $mediaRefFieldFileEntity->getMimeType(),
  ];
  $mediaRefFieldFileAlt = $mediaRefFieldEntity->get('field_media_image')->alt;
  $mediaRefFieldFileTitle = $mediaRefFieldEntity->get('field_media_image')->title;
}
```

Let's take a look at the equivalent of getting all the data above using a
helper.

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $helper = \Drupal\glint\Glint::helper($variables['node']);
  $media = $helper->get('field_featured_image'); // File reference field.
}
```

And a preview of what this data would look like if dumped:

![Media Glint Data](./docs/screenshots/media-field-glint.png)

As you can see, the data here is much easier to work with. This helps save time
and also softens the learning curve for new devs and/or frontend experts.

Glint goes a step further with multi-value entity reference fields with the
addition of [Entity Helper Collections](#entity-helper-collections). Read more about them below!

### Entity Helper Collections

In the case where you use Glint to obtain data from an **Entity Reference**
field that allows **multiple values**, you will come across an
**Entity Helper Collection**.

This class is an [iterable](https://www.php.net/manual/en/language.types.iterable.php) object that contains helpers for all referenced
entities in the field. It contains some handy helper methods that will be
showcased below.

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function YOUR_THEME_preprocess_node(&$variables) {
  $node = $variables['node'];
  $helper = \Drupal\glint\Glint::helper($node);

  // This gives us a GlintEntityHelperCollection class.
  $relatedPagesCollection = $helper->get('field_related_pages');

  // Get the first, second, nth elements.
  $firstRelatedPage = $relatedPagesCollection->first();
  $secondRelatedPage = $relatedPagesCollection->second();
  $thirdRelatedPage = $relatedPagesCollection->third();
  $fourthRelatedPage = $relatedPagesCollection->fourth();
  $tenthRelatedPage = $relatedPagesCollection->index(9);

  // Get third related page's body.
  $thirdRelatedPageBody = $thirdRelatedPage->get('body');

  // Get the 'body' field value for all related pages.
  // This will return the values cleaned by Glint.
  $allBodyFieldValues = $relatedPagesCollection->harvest('body');

  // Alternatively, get all body field values, but unaltered by Glint.
  // This would be the equivalent of looping through all referenced entities and calling
  // $entity->get('body')->getValue().
  $allBodyFieldValuesRaw = $relatedPagesCollection->harvestRaw('body');

  // Simulate an array_map(), executing a callback on all helpers in the collection.
  // Here we get all the node IDs of the related pages.
  $allRelatedPageIds = $relatedPagesCollection->map(function($helper) {
    return $helper->getEntity()->id();
  });

  // You can also loop through the collection as if it were an array.
  foreach ($relatedPagesCollection as $nodeHelper) {
    $variables['featured_images'][] = $nodeHelper->get('field_featured_image');
  }
}
```

Collections are meant to simplify access to data of multi-value reference
fields. Use them to your liking!

## Configuration

All relevant configuration for the Glint module can be found at
`/admin/config/glint`.

Configurations here influence the data returned when Glint performs basic data
cleaning.

### Skip Arrays for Single Value Fields

Traditionally in Drupal, when calling `->getValue()` on a field from an entity,
you **always** get an array, regardless of the field cardinality.

If this is something you wish to clean up when using Glint, you can activate
this option.

When Glint detects that a field defined for an entity only allows 1 value (has
cardinality set to 1), the singular value is returned instead of an array
containing only 1 value.

### Allow Alter Hooks

This functionality allows [Alter Hooks](#glint-alter-hooks) to modify the data returned by
Glint. You can read more about this below.

## Advanced Features

### Glint Service

The Glint service can be called from anywhere in your code to obtain a field
value from an entity.

```php
$node = Node::load(23);
$nodeBody = \Drupal::service('glint')->get('body', $node);
```

This service can also be used as a shortcut to build a helper for a node.

```php
$node = Node::load(23);
$nodeHelper = \Drupal::service('glint')->helper($node);
$nodeBody = $nodeHelper->get('field_media_reference');
```

Finally, the `\Drupal\glint\Glint` service class has a couple of static
functions you can use as shortcuts.

```php
$node = Node::load(23);
$nodeBody = Glint::service()->get('body', $node);

$nodeHelper = Glint::helper($node);
$nodeBody = $nodeHelper->get('field_media_reference');
```

If using Glint in the context of a module, it is always preferable to use
[Dependency Injection](https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8) to access the service. The static functions exist to
facilitate access to the service from within themes and hooks.

### Glint Alter Hooks

When you obtain field values from entities using Glint, the module starts by
getting the default field data. From there, it extracts the relevant data and
sometimes does additional lookups to get more relevant data that Drupal doesn't
give by default.

This process is referred to as data extraction in Glint. For advanced
developers, this process can be fine-tuned with simple hooks that allow you to
further alter the data returned by Glint.

```php
/**
 * Implements hook_glint_field_extract_TYPE_alter().
 */
function YOUR_MODULE_glint_field_extract_boolean_alter(&$value, $originalValue, array $info) {
  $value = $value ? 'TRUE' : 'FALSE';
}

/**
 * Implements hook_glint_field_extract_TYPE_alter().
 */
function YOUR_THEME_glint_field_extract_text_with_summary_alter(&$value, $originalValue, array $info) {
  $value['exploded_text'] = explode(' ', $value['text']);
}
```

Using these hooks only affects the data returned when using
[Helpers](#entity-helpers) or the [Glint Service](#glint-service).

It is important to recognize that modifications through these hooks will change
how data is returned on **all instances of using Glint**. This feature is meant
to be used as a way to slightly modify how data is extracted from fields.

Keep this in mind when using this feature!

## Roadmap

* TBD.
