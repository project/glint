<?php

/**
 * @file
 * Hooks for the glint module.
 */

/**
 * Allows modules & themes to alter the field values returned from Glint.
 *
 * TYPE here refers to the field type.
 *
 * Glint provides basic extraction of field values from entity fields.
 *
 * Providing hooks allows us to fine-tune this default behaviour.
 *
 * @param mixed $value
 *   The value to alter. This value has already gone through basic manipulations
 *   by Glint.
 * @param array $originalValue
 *   The original value without Glint cleanup.
 * @param array $info
 *   Information about the entity to get the value from and the item list.
 */
function hook_glint_field_extract_TYPE_alter(mixed &$value, array $originalValue, array $info) : void {
  $value = 'foo';
}
