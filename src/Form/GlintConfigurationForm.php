<?php

namespace Drupal\glint\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\glint\Glint;

/**
 * Provides a configuration form for the Glint module.
 *
 * @package Drupal\glint\Form
 *
 * @noinspection PhpUnused
 */
class GlintConfigurationForm extends ConfigFormBase {

  /**
   * The FORM ID.
   *
   * @var string
   */
  public const FORM_ID = 'glint_configuration_form';

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      Glint::CONFIG_ID,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Set this for easier organization of form values later.
    $form['#tree'] = TRUE;

    // Get the current configurations.
    $skipArrays = $this->config(Glint::CONFIG_ID)->get(Glint::CONFIG__SKIP_ARRAYS);
    $cleanAttributesArray = $this->config(Glint::CONFIG_ID)->get(Glint::CONFIG__CLEAN_ATTRIBUTES_ARRAY);
    $allowAlterHooks = $this->config(Glint::CONFIG_ID)->get(Glint::CONFIG__ALLOW_ALTER_HOOKS);

    $form[Glint::CONFIG__SKIP_ARRAYS] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Skip arrays for single value fields'),
      '#description'   => $this->t('If this is enabled, Glint will return single values instead of arrays for fields that only allow 1 value.'),
      '#default_value' => !empty($skipArrays) ? $skipArrays : 0,
    ];

    $form[Glint::CONFIG__CLEAN_ATTRIBUTES_ARRAY] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Clean <code>_attributes</code> entry when extracting values'),
      '#description'   => $this->t('If this is enabled, Glint will return values without the <code>_attributes</code> array key when calling <code>->getValue()</code> on field item lists.'),
      '#default_value' => !empty($cleanAttributesArray) ? $cleanAttributesArray : 0,
    ];

    $form[Glint::CONFIG__ALLOW_ALTER_HOOKS] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Allow alter hooks'),
      '#description'   => $this->t('If this is enabled, you will be able to alter returned field data with hooks. Refer to the <em>glint.api.php</em> file for more details.'),
      '#default_value' => !empty($allowAlterHooks) ? $allowAlterHooks : 0,
    ];

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    // Save configurations.
    $this->configFactory->getEditable(Glint::CONFIG_ID)
      ->set(Glint::CONFIG__SKIP_ARRAYS, $values[Glint::CONFIG__SKIP_ARRAYS])
      ->set(Glint::CONFIG__CLEAN_ATTRIBUTES_ARRAY, $values[Glint::CONFIG__CLEAN_ATTRIBUTES_ARRAY])
      ->set(Glint::CONFIG__ALLOW_ALTER_HOOKS, $values[Glint::CONFIG__ALLOW_ALTER_HOOKS])
      ->save();

    // Default submission handler.
    parent::submitForm($form, $form_state);
  }

}
