<?php

namespace Drupal\glint;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for the Glint service.
 */
interface GlintInterface {

  /**
   * Get main configuration for the Glint module.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The immutable configuration object.
   */
  public function config() : ImmutableConfig;

  /**
   * Get the value of a field for a given entity.
   *
   * The output of this function is meant to be cleaner than the traditional
   * ItemList->getValue().
   *
   * @param string $fieldName
   *   Name of the field to get the value for.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field value from.
   *
   * @return mixed
   *   The returned field value, cleaned of any noise.
   */
  public function get(string $fieldName, ContentEntityInterface $entity) : mixed;

}
