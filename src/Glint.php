<?php

namespace Drupal\glint;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\glint\FieldValueCleaner\AddressFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\DateRangeFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\DefaultFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\EntityReferenceFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\EntityReferenceRevisionsFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\FileFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\ImageFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\LinkFieldValueCleaner;
use Drupal\glint\FieldValueCleaner\TextWithSummaryFieldValueCleaner;

/**
 * Provides a service for Glint.
 *
 * This is the main entrypoint to the Glint toolkit.
 */
final class Glint implements GlintInterface {
  use StringTranslationTrait;

  /**
   * Supported field types by default.
   *
   * Field types should be added to this array when supported by the module by
   * default.
   *
   * @see self::extract()
   *
   * @var array
   */
  private const SUPPORTED_FIELD_TYPES = [
    'address',
    'address_country',
    'boolean',
    'datetime',
    'daterange',
    'decimal',
    'email',
    'entity_reference',
    'entity_reference_revisions',
    'file',
    'float',
    'image',
    'integer',
    'link',
    'list_float',
    'list_integer',
    'list_string',
    'telephone',
    'text',
    'text_long',
    'text_with_summary',
    'timestamp',
    'string',
    'string_long',
  ];

  /**
   * The ID of the main configuration for the module.
   *
   * @var string
   */
  public const CONFIG_ID = 'glint.settings';

  /**
   * ID of the 'Skip Arrays' configuration.
   *
   * Makes Glint return a singular element when fetching data from entity fields
   * limited to 1 value.
   *
   * @var string
   */
  public const CONFIG__SKIP_ARRAYS = 'skip_arrays';

  /**
   * ID of the 'Clean Attributes' configuration.
   *
   * Makes Glint remove the '_attributes' key when fetching field values.
   *
   * @var string
   */
  public const CONFIG__CLEAN_ATTRIBUTES_ARRAY = 'clean_attributes_array';

  /**
   * ID of the 'Allow Alter Hooks' configuration.
   *
   * Allows hooks to alter the data extracted from fields by Glint.
   *
   * @see glint.api.php
   *
   * @var string
   */
  public const CONFIG__ALLOW_ALTER_HOOKS = 'allow_alter_hooks';

  /**
   * Use DI to inject Drupal's configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public ConfigFactoryInterface $configFactory;

  /**
   * Drupal's module handler injected through DI.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public ModuleHandlerInterface $moduleHandler;

  /**
   * Drupal's Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  public ThemeManagerInterface $themeManager;

  /**
   * Drupal's Theme Extension List injected through DI.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  public ThemeExtensionList $themeExtensionList;

  /**
   * Drupal's Entity Repository injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  public EntityRepositoryInterface $entityRepository;

  /**
   * Drupal's Messenger injected through DI.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  public MessengerInterface $messenger;

  /**
   * Logger Channel injected through DI.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  public LoggerChannelInterface $loggerChannel;

  /**
   * Constructs the Glint service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration Factory service injected through DI.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module Handler service injected through DI.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme Manager service injected through DI.
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *   Theme Extension List service injected through DI.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   Entity Repository injected through DI.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service injected through DI.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   Logger Channel injected through DI.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ModuleHandlerInterface $moduleHandler,
    ThemeManagerInterface $themeManager,
    ThemeExtensionList $themeExtensionList,
    EntityRepositoryInterface $entityRepository,
    MessengerInterface $messenger,
    LoggerChannelInterface $loggerChannel
  ) {
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
    $this->themeManager = $themeManager;
    $this->themeExtensionList = $themeExtensionList;
    $this->entityRepository = $entityRepository;
    $this->messenger = $messenger;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * Shortcut to obtain the Glint service from the global container.
   *
   * @return \Drupal\glint\GlintInterface
   *   The Glint service.
   */
  public static function service() : GlintInterface {
    static $glint;
    if (!empty($glint)) {
      return $glint;
    }
    $glint = \Drupal::service('glint');
    return $glint;
  }

  /**
   * Get a helper for an entity.
   *
   * Helpers allow ease of access to entity fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to instantiate a helper for.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface
   *   The entity helper.
   */
  public static function helper(ContentEntityInterface $entity) : GlintEntityHelperInterface {
    return new GlintEntityHelper(Glint::service(), $entity);
  }

  /**
   * {@inheritDoc}
   */
  public function config() : ImmutableConfig {
    static $config = NULL;
    if (!empty($config)) {
      return $config;
    }
    $config = $this->configFactory->get(self::CONFIG_ID);
    return $config;
  }

  /**
   * {@inheritDoc}
   */
  public function get(string $fieldName, ContentEntityInterface $entity, GlintEntityHelper $parentHelper = NULL) : mixed {
    // Ensure the entity has the right translation.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entityRepository->getTranslationFromContext($entity);

    // Get the item list.
    $itemList = $entity->get($fieldName);

    // We can stop here if the field is empty.
    if ($itemList->isEmpty()) {
      return [];
    }

    // Load our toolset.
    $fieldDefinition = $itemList->getFieldDefinition();
    $type = $fieldDefinition->getType();
    $cardinality = $fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $cleanValues = [];

    // Loop in our array of values. Drupal will always give us an array.
    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    foreach ($itemList as $item) {
      // Extract the value using Glint. This does an initial cleanup of the
      // value, but nothing drastic.
      $cleanValues[] = $this->clean($type, $item->getValue(), [
        'entity' => $entity,
        'item' => $item,
        'parent' => $parentHelper,
      ]);
    }

    // If the field cardinality is 1, we can return just the sole value.
    // Only do this if it's configured.
    if ($cardinality === 1 && self::config()->get(self::CONFIG__SKIP_ARRAYS)) {
      return $cleanValues[0];
    }

    // If we have multiple cardinality, and we're dealing with entity references
    // return a GlintEntityHelperCollection.
    $helperFieldTypes = [
      'entity_reference',
      'entity_reference_revisions',
    ];
    if (($cardinality > 1 || $cardinality < 0) && in_array($type, $helperFieldTypes)) {
      $excludedEntityTypes = [
        'media',
        'taxonomy_term',
      ];
      $targetType = $fieldDefinition->getFieldStorageDefinition()->getSetting('target_type');
      if (!in_array($targetType, $excludedEntityTypes)) {
        return new GlintEntityHelperCollection($this, $cleanValues);
      }
    }

    // Return the cleaned value.
    return $cleanValues;
  }

  /**
   * Extract relevant data from a field value, cleaning noise.
   *
   * Given the field type, Glint will perform specific cleanup.
   * That being said, the cleanup should not be drastic.
   *
   * @param string $type
   *   The field type to extract from.
   * @param array $value
   *   The original value obtained from ->getValue() on the ItemList.
   * @param array $info
   *   Information about the entity to get the value from and the item list.
   *
   * @return mixed
   *   The extracted value.
   */
  public function clean(string $type, array $value, array $info) : mixed {
    // If the field type is not supported, then Glint shouldn't be used for it,
    // as it will not change a thing.
    // Throw a warning in this case and simply return the unaltered value.
    if (!in_array($type, self::SUPPORTED_FIELD_TYPES)) {
      $this->messenger->addWarning($this->t('Fields of type {@type} are not currently supported by Glint. You should access the field by standard Drupal methods.', [
        '@type' => $type,
      ]));
      return $value;
    }

    // Store original value in case we modify it.
    $originalValue = $value;

    // Clean '_attributes' from the value if we must.
    if ($this->config()->get(self::CONFIG__CLEAN_ATTRIBUTES_ARRAY) && isset($value['_attributes'])) {
      unset($value['_attributes']);
    }

    // Get our cleaned value.
    // We want to avoid huge changes here. KISS!
    $cleanedValue = match ($type) {
      'address' => AddressFieldValueCleaner::clean($value),
      'daterange' => DateRangeFieldValueCleaner::clean($value),
      'entity_reference' => EntityReferenceFieldValueCleaner::clean($value, $info['item'], $this, $this->entityRepository, $info['parent']),
      'entity_reference_revisions' => EntityReferenceRevisionsFieldValueCleaner::clean($value, $info['item'], $this, $this->entityRepository, $info['parent']),
      'file' => FileFieldValueCleaner::clean($value, $info['item']),
      'image' => ImageFieldValueCleaner::clean($value, $info['item']),
      'link' => LinkFieldValueCleaner::clean($value),
      'text_with_summary' => TextWithSummaryFieldValueCleaner::clean($value),
      default => DefaultFieldValueCleaner::clean($value),
    };

    // Run alter hooks if we must.
    if ($this->config()->get(self::CONFIG__ALLOW_ALTER_HOOKS)) {
      $this->moduleHandler->alter("glint_field_extract_{$type}", $cleanedValue, $originalValue, $info);
      $this->themeManager->alter("glint_field_extract_{$type}", $cleanedValue, $originalValue, $info);
    }

    return $cleanedValue;
  }

}
