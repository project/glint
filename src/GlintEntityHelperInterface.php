<?php

namespace Drupal\glint;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides the GlintEntityHelperInterface interface.
 */
interface GlintEntityHelperInterface {

  /**
   * Get the title of the attached entity.
   *
   * @return string
   *   The title of the entity.
   */
  public function title() : string;

  /**
   * Get the title of the attached entity.
   *
   * @return string
   *   The title of the entity.
   */
  public function label() : string;

  /**
   * Get the bundle of the attached entity.
   *
   * @return string|NULL
   *   The bundle of the entity.
   */
  public function bundle() : ?string;

  /**
   * Shortcut to access the bundle.
   *
   * Sometimes people think "type" instead of "bundle".
   *
   * @return string|NULL
   *   The bundle of the entity.
   */
  public function type() : ?string;

  /**
   * Get the attached entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity attached to this helper.
   */
  public function getEntity() : ContentEntityInterface;

  /**
   * Get a field value from an entity with Glint cleanup.
   *
   * @param string $fieldName
   *   Field name of the field we want to get a value for.
   *
   * @return mixed
   *   The field value, cleaned by Glint.
   */
  public function get(string $fieldName) : mixed;

  /**
   * Get all field values from an entity with Glint cleanup.
   *
   * @return array
   *   The field values, cleaned by Glint.
   */
  public function all(): array;

  /**
   * Get a field value from an entity without any additional manipulations.
   *
   * @param string $fieldName
   *   Field name of the field we want to get a value for.
   *
   * @return mixed
   *   The raw field value.
   */
  public function getRaw(string $fieldName) : mixed;

  /**
   * Get a field value from an entity without any additional manipulations.
   *
   * @return array
   *   The raw field values.
   */
  public function allRaw() : array;

}
