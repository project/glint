<?php

namespace Drupal\glint;

/**
 * Provides the GlintEntityHelperCollection class.
 *
 * A simple class to facilitate access to a list of GlintEntityHelper objects.
 */
class GlintEntityHelperCollection implements GlintEntityHelperCollectionInterface, \Iterator {

  /**
   * The main Glint service.
   *
   * @var \Drupal\glint\GlintInterface
   */
  private GlintInterface $glint;

  /**
   * Array of all helpers in this collection.
   *
   * @var \Drupal\glint\GlintEntityHelper[]
   */
  private array $items;

  /**
   * The pointer.
   *
   * @var int
   */
  private int $pointer = 0;

  /**
   * Constructs a GlintEntityHelper object.
   *
   * @param \Drupal\glint\Glint $glint
   *   The Glint service.
   * @param \Drupal\glint\GlintEntityHelper[] $items
   *   The array of helpers this collection will access.
   */
  public function __construct(Glint $glint, array $items) {
    $this->glint = $glint;
    $this->items = $items;
  }

  /**
   * {@inheritDoc}
   */
  public function current() : GlintEntityHelper {
    return $this->items[$this->pointer];
  }

  /**
   * {@inheritDoc}
   */
  public function key() : int {
    return $this->pointer;
  }

  /**
   * {@inheritDoc}
   */
  public function next() : void {
    $this->pointer++;
  }

  /**
   * {@inheritDoc}
   */
  public function rewind() : void {
    $this->pointer = 0;
  }

  /**
   * {@inheritDoc}
   */
  public function valid() : bool {
    return $this->pointer < count($this->items);
  }

  /**
   * {@inheritDoc}
   */
  public function toArray() : array {
    return $this->items;
  }

  /**
   * {@inheritDoc}
   */
  public function first() : ?GlintEntityHelperInterface {
    return $this->items[0] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function second() : ?GlintEntityHelperInterface {
    return $this->items[1] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function third() : ?GlintEntityHelperInterface {
    return $this->items[2] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function fourth() : ?GlintEntityHelperInterface {
    return $this->items[3] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function fifth() : ?GlintEntityHelperInterface {
    return $this->items[4] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function index(int $i) : ?GlintEntityHelperInterface {
    return $this->items[$i] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function map(\Closure $callback) : array {
    return array_map($callback, $this->items);
  }

  /**
   * {@inheritDoc}
   */
  public function harvest(string $fieldName) : array {
    $fields = [];
    foreach ($this as $helper) {
      $fields[] = $helper->get($fieldName);
    }
    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function harvestRaw(string $fieldName) : array {
    $fields = [];
    foreach ($this as $helper) {
      $fields[] = $helper->getEntity()->get($fieldName)->getValue();
    }
    return $fields;
  }

}
