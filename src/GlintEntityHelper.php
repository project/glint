<?php

namespace Drupal\glint;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides the GlintEntityHelper class.
 */
final class GlintEntityHelper implements GlintEntityHelperInterface {

  /**
   * The main Glint service.
   *
   * @var \Drupal\glint\GlintInterface
   */
  private GlintInterface $glint;

  /**
   * The parent entity helper if any.
   *
   * @var \Drupal\glint\GlintEntityHelperInterface|null
   */
  private ?GlintEntityHelperInterface $parent;

  /**
   * The entity attached to this helper.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  public ContentEntityInterface $entity;

  /**
   * Constructs a GlintEntityHelper object.
   *
   * @param \Drupal\glint\Glint $glint
   *   The Glint service.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity attached to this helper.
   * @param \Drupal\glint\GlintEntityHelperInterface $parent
   * The parent helper if any.
   */
  public function __construct(Glint $glint, ContentEntityInterface $entity, GlintEntityHelperInterface $parent = NULL) {
    $this->glint = $glint;
    $this->entity = $entity;
    $this->parent = $parent;
  }

  public function getParent() : ?GlintEntityHelperInterface {
    return $this->parent;
  }

  public function depth() : int {
    $depth = 0;
    $parent = $this->parent;
    while ($parent !== NULL) {
      $depth++;
      $parent = $parent->getParent();
    }
    return $depth;
  }

  /**
   * {@inheritDoc}
   */
  public function title() : string {
    return $this->label();
  }

  /**
   * {@inheritDoc}
   */
  public function label() : string {
    return $this->entity->label();
  }

  /**
   * {@inheritDoc}
   */
  public function bundle() : ?string {
    return $this->entity->bundle();
  }

  /**
   * {@inheritDoc}
   */
  public function type() : ?string {
    return $this->bundle();
  }

  /**
   * {@inheritDoc}
   */
  public function getEntity() : ContentEntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritDoc}
   */
  public function get(string $fieldName) : mixed {
    return $this->glint->get($fieldName, $this->entity, $this);
  }

  /**
   * {@inheritDoc}
   */
  public function all(array $except = []) : array {
    $data = [];
    $fields = $this->entity->getFields();
    foreach (array_keys($fields) as $fieldName) {
      if (!in_array($fieldName, $except) && (str_starts_with($fieldName, 'field_') || $fieldName === 'body')) {
        $data[$fieldName] = $this->get($fieldName);
      }
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getRaw(string $fieldName) : mixed {
    return $this->entity->get($fieldName)->getValue();
  }

  /**
   * {@inheritDoc}
   */
  public function allRaw() : array {
    return array_map(function ($itemList) {
      return $itemList->getValue();
    }, $this->entity->getFields());
  }

}
