<?php

namespace Drupal\glint\FieldValueCleaner;

/**
 * Provides DefaultFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for basic fields.
 */
final class DefaultFieldValueCleaner {

  /**
   * Clean up value for a simple field.
   *
   * @param array $value
   *   The original value from Drupal.
   *   Usually a simple array.
   *   e.g. ['value' => 'The value', '_attributes' => []].
   *
   * @return mixed
   *   Simple value array.
   */
  public static function clean(array $value) : mixed {
    return $value['value'] ?? $value;
  }

}
