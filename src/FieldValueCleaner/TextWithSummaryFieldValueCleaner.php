<?php

namespace Drupal\glint\FieldValueCleaner;

/**
 * Provides TextFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'text_with_summary' type fields.
 */
final class TextWithSummaryFieldValueCleaner {

  /**
   * Clean up value for a 'text_with_summary' type field.
   *
   * @param array $value
   *   The original value from Drupal.
   *
   * @return array
   *   Simple value array.
   */
  public static function clean(array $value) : array {
    return [
      'text' => $value['value'],
      'summary' => $value['summary'] ?? '',
    ];
  }

}
