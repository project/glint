<?php

namespace Drupal\glint\FieldValueCleaner;

use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Provides ImageFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'image' type fields.
 *
 * @param array $value
 *   The original value from Drupal.
 * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $item
 *   The image item from the field value.
 *
 * @return array
 *   Simple value array.
 */
final class ImageFieldValueCleaner {

  /**
   * Provides FileFieldValueCleaner class.
   *
   * Handles default Glint cleanup tasks for 'file' type fields.
   */
  public static function clean(array $value, ImageItem $item) : array {
    // We are currently accessing the property via magic method.
    // This may get updated in the future, but we're safe to do this for now.
    /** @var \Drupal\file\Entity\File $fileEntity */
    $fileEntity = $item->entity;

    $value = [
      'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($fileEntity->getFileUri()),
      'uri' => $fileEntity->getFileUri(),
      'filename' => $fileEntity->getFilename(),
      'metadata' => [
        'filesize' => $fileEntity->getSize(),
        'mimetype' => $fileEntity->getMimeType(),
      ],
    ];

    // Add attributes.
    $value['attributes'] = [
      'alt' => $item->alt,
      'title' => $item->title,
    ];

    // Add extra metadata.
    $value['metadata']['width'] = $item->width;
    $value['metadata']['height'] = $item->height;
    return $value;
  }

}
