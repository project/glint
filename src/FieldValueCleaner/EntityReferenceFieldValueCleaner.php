<?php

namespace Drupal\glint\FieldValueCleaner;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\glint\Glint;
use Drupal\glint\GlintEntityHelper;
use Drupal\glint\GlintEntityHelperInterface;
use Drupal\glint\GlintInterface;
use Drupal\media\MediaInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Provides EntityReferenceFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'entity_reference' type fields.
 */
class EntityReferenceFieldValueCleaner {

  /**
   * Clean up value for an 'entity_reference' type field.
   *
   * @param array $value
   *   The original value from Drupal.
   * @param \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $item
   *   The item.
   * @param \Drupal\glint\GlintInterface $glint
   *   The Glint service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The Entity Repository service.
   *
   * @return \Drupal\glint\GlintEntityHelper|array
   *   A helper class to facilitate access to entity fields.
   */
  public static function clean(
    array $value,
    EntityReferenceItem $item,
    GlintInterface $glint,
    EntityRepositoryInterface $entityRepository,
    GlintEntityHelperInterface $parent = NULL,
  ) : GlintEntityHelper | array {
    $entity = $item->entity;
    $entity = $entityRepository->getTranslationFromContext($entity);

    // For certain entity types, Glint will return a simple array instead of a
    // helper.
    return match ($entity->getEntityTypeId()) {
      'taxonomy_term' => self::cleanTaxonomyTermEntity($glint, $entity, $parent),
      'media' => self::cleanMediaEntity($glint, $entity, $parent),
      default => new GlintEntityHelper($glint, $entity, $parent),
    };
  }

  /**
   * Perform cleaning for Media entities.
   *
   * These entities are generally simple and have common data.
   *
   * @param \Drupal\glint\GlintInterface $glint
   *   The Glint service.
   * @param \Drupal\media\MediaInterface $entity
   *   The media entity.
   *
   * @return array|GlintEntityHelperInterface
   *   Array of relevant data from the media.
   */
  private static function cleanMediaEntity(GlintInterface $glint, MediaInterface $entity, GlintEntityHelperInterface $parent) : array | GlintEntityHelperInterface {
    // Get a helper for the media entity.
    $helper = new GlintEntityHelper($glint, $entity, $parent);

    // Include the media title.
    $values['media_title'] = $entity->label();

    // Get the source field we're going to use.
    $sourceField = $entity->getSource()->getConfiguration()['source_field'] ?? NULL;
    if (empty($sourceField)) {
      return $helper;
    }

    // Get value of the source field.
    $sourceFieldValue = $helper->get($sourceField);

    // We ain't going far.
    if ($helper->depth() > 2) {
      $values['helper'] = $helper;
    } else {
      // Load values of all fields for the media item.
      $values['fields'] = $helper->all([$sourceField]);
    }

    // Merge values of the source field into the first level of the array.
    if (!empty($sourceFieldValue)) {
      $data = $sourceFieldValue;
      // If we don't skip arrays well we skip them here.
      if (!$glint->config()->get(Glint::CONFIG__SKIP_ARRAYS)) {
        $data = $data[0];
      }
      if (is_array($data)) {
        $values = array_merge($data, $values);
      }
      else {
        $values[$sourceField] = $data;
      }
    }

    // Additional actions for remote videos.
    if ($entity->getSource()->getPluginId() === 'oembed:video') {
      $youtubeRegex = '/^(https?:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/';
      $vimeoRegex = '/vimeo\.com\/\d+/';
      $matches = [];
      $idRegex = NULL;

      if (preg_match($youtubeRegex, $values[$sourceField])) {
        $values['provider'] = 'youtube';
        $idRegex = '/^.*(?:youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*$/i';
      }

      if (preg_match($vimeoRegex, $values[$sourceField])) {
        $values['provider'] = 'vimeo';
        $idRegex = '/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/[^\/]*\/videos\/|album\/\d+\/video\/|video\/|)(\d+)(?:[a-zA-Z\d_\-]+)?/i';
      }

      if ($idRegex !== NULL) {
        preg_match($idRegex, $values[$sourceField], $matches);
        $values['video_id'] = $matches[1] ?? NULL;
      }

      if (!empty($values[$sourceField])) {
        $url = $values[$sourceField];
        unset($values[$sourceField]);
        $values['url'] = $url;
      }
    }

    return $values;
  }

  /**
   * Perform cleaning for Taxonomy Term entities.
   *
   * These entities are generally simple and have common data.
   *
   * @param \Drupal\glint\GlintInterface $glint
   *   The Glint service.
   * @param \Drupal\taxonomy\TermInterface $entity
   *   The term entity.
   *
   * @return array|GlintEntityHelperInterface
   *   Array of relevant data from the taxonomy term.
   */
  private static function cleanTaxonomyTermEntity(GlintInterface $glint, TermInterface $entity, GlintEntityHelperInterface $parent) : array|GlintEntityHelperInterface {
    // Get a glint output of all fields in the term.
    $helper = new GlintEntityHelper($glint, $entity, $parent);

    // We ain't going far.
    if ($helper->depth() > 2) {
      return $helper;
    }

    $values = $helper->all();

    // Include the term title & href.
    $values['title'] = $entity->label();
    try {
      $values['href'] = $entity->toUrl()->toString();
    }
    catch (EntityMalformedException $e) {
      $values['href'] = NULL;
    }

    return $values;
  }

  /**
   * Return all glinted fields from entity.
   *
   * These entities are generally simple and have common data.
   *
   * @param \Drupal\glint\GlintInterface $glint
   *   The Glint service.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return array
   *   Array of relevant data from the entity.
   */
  private static function returnAll(GlintInterface $glint, ContentEntityInterface $entity) : array {
    $helper = new GlintEntityHelper($glint, $entity);
    return $helper->all();
  }

}
