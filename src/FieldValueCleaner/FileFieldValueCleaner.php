<?php

namespace Drupal\glint\FieldValueCleaner;

use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Provides FileFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'file' type fields.
 */
final class FileFieldValueCleaner {

  /**
   * Clean up value for an 'file' type field.
   *
   * @param array $value
   *   The original value from Drupal.
   * @param \Drupal\file\Plugin\Field\FieldType\FileItem $item
   *   The file item from the field value.
   *
   * @return array
   *   Simple value array.
   */
  public static function clean(array $value, FileItem $item) : array {
    // We are currently accessing the property via magic method.
    // This may get updated in the future, but we're safe to do this for now.
    /** @var \Drupal\file\Entity\File $fileEntity */
    $fileEntity = $item->entity;

    return [
      'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($fileEntity->getFileUri()),
      'uri' => $fileEntity->getFileUri(),
      'filename' => $fileEntity->getFilename(),
      'metadata' => [
        'filesize' => $fileEntity->getSize(),
        'mimetype' => $fileEntity->getMimeType(),
      ],
    ];
  }

}
