<?php

namespace Drupal\glint\FieldValueCleaner;

/**
 * Provides EntityReferenceRevisionsFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'entity_reference_revisions' type
 * fields.
 */
final class EntityReferenceRevisionsFieldValueCleaner extends EntityReferenceFieldValueCleaner {}
