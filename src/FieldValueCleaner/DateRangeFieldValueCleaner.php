<?php

namespace Drupal\glint\FieldValueCleaner;

/**
 * Provides DateRangeFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'daterange' type fields.
 */
final class DateRangeFieldValueCleaner {

  /**
   * Clean up value for an 'daterange' type field.
   *
   * @param array $value
   *   The original value from Drupal.
   *
   * @return array
   *   Simple value array.
   */
  public static function clean(array $value) : array {
    return [
      'start' => $value['value'],
      'end' => $value['end_value'],
    ];
  }

}
