<?php

namespace Drupal\glint\FieldValueCleaner;

/**
 * Provides AddressFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'address' type fields.
 */
final class AddressFieldValueCleaner {

  /**
   * Clean up value for an 'address' type field.
   *
   * @param array $value
   *   The original value from Drupal.
   *
   * @return array
   *   Simple value array.
   */
  public static function clean(array $value) : array {
    return $value;
  }

}
