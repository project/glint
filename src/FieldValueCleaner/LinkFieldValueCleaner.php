<?php

namespace Drupal\glint\FieldValueCleaner;

/**
 * Provides LinkFieldValueCleaner class.
 *
 * Handles default Glint cleanup tasks for 'link' type fields.
 */
final class LinkFieldValueCleaner {

  /**
   * Clean up value for a 'link' type field.
   *
   * @param array $value
   *   The original value from Drupal.
   *
   * @return array
   *   Simple value array.
   */
  public static function clean(array $value) : array {
    return [
      'href' => $value['uri'],
      'label' => $value['title'] ?? '',
      'attributes' => $value['options']['attributes'] ?? [],
    ];
  }

}
