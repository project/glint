<?php

namespace Drupal\glint;

/**
 * Provides the GlintEntityHelperCollectionInterface interface.
 */
interface GlintEntityHelperCollectionInterface {

  /**
   * Return the array of items in this collection.
   *
   * @return array
   *   The array of items.
   */
  public function toArray() : array;

  /**
   * Return the first item.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface|null
   *   The first item in the list.
   */
  public function first() : ?GlintEntityHelperInterface;

  /**
   * Return the second item.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface|null
   *   The first item in the list.
   */
  public function second() : ?GlintEntityHelperInterface;

  /**
   * Return the third item.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface|null
   *   The first item in the list.
   */
  public function third() : ?GlintEntityHelperInterface;

  /**
   * Return the fourth item.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface|null
   *   The first item in the list.
   */
  public function fourth() : ?GlintEntityHelperInterface;

  /**
   * Return the fifth item.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface|null
   *   The first item in the list.
   */
  public function fifth() : ?GlintEntityHelperInterface;

  /**
   * Return the item at the given index.
   *
   * @return \Drupal\glint\GlintEntityHelperInterface|null
   *   The item in the list at the specified index.
   */
  public function index(int $i) : ?GlintEntityHelperInterface;

  /**
   * Simulate an array_map on this collection.
   *
   * Run a provided callback on all items and return the result.
   *
   * @return array
   *   Array of results of the executed callback on each collection item.
   */
  public function map(\Closure $callback) : array;

  /**
   * Get the value for the given field from all items.
   *
   * @param string $fieldName
   *   The field to harvest from all items.
   *
   * @return array
   *   Array of field values from all items.
   */
  public function harvest(string $fieldName) : array;

  /**
   * Get the raw value for the given field from all items.
   *
   * Raw means unaltered by Glint.
   *
   * @param string $fieldName
   *   The field to harvest from all items.
   *
   * @return array
   *   Array of field values from all items.
   */
  public function harvestRaw(string $fieldName) : array;

}
